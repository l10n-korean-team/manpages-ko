#!/bin/sh
set -e

mkdir manpages-ko-$DATE
cd manpages-ko-$DATE
wget -O man.tar.gz "http://man.kldp.org/down/man-pages-ko-$DATE.tar.gz"
cd ..
tar cvf - manpages-ko-$DATE | gzip -9c > manpages-ko-$DATE.tar.gz

