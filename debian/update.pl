#!/usr/bin/perl -w


unless (-d "debian" && -f "debian/rules") {
    print "데비안 디렉토리가 아닌 것 같은데요... :p\n";
    exit 1;
}

$date = `LANG=C date +%Y%m%d`; chop $date;
if ($#ARGV >= 0) {
    $version = $ARGV[0];
} else {
    $version = $date;
}
#$version = "20001007";
$dirname = "manpages-ko-$version";
mkdir $dirname, 0755;
chdir $dirname;

$wget_command = "wget -O man.tar.gz \"http://man.kldp.org/down/man-pages-ko-$version.tar.gz\"";

$ret = system($wget_command);
if ($ret != 0) {
    print "wget 실패\n";
    exit 1;
}
chdir "..";
system("tar zcvf $dirname.tar.gz $dirname");
system("rm -rf $dirname");
system("mv $dirname.tar.gz ..");

system("uupdate ../$dirname.tar.gz $version");

exit 0;

